export class Login {
  id: number;
  mail: string;
  password: string;
  sid: string;
  sid_limit: Date;
  last_login: Date;
  user_type: number;
  del_flg: number;
  creator_div: number;
  creator_id: number;
  create_date: Date;
  updater_div: number;
  updater_id: number;
  update_date: Date;
}
