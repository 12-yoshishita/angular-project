// コアからコンポーネントのインポート
import { Component } from '@angular/core';

@Component({
  // HTMLの出力先の指定
  selector: 'app-root',
  // HTMLテンプレートファイルの指定
  templateUrl: './app.component.html',
  // CSSファイルの指定
  styleUrls: ['./app.component.scss']
})

// コンポーネントクラスの定義
export class AppComponent {
  // 変数のtitleの宣言と代入
  title = 'angular-project';
}
