// モジュールのインポート
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule, HttpClientXsrfModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { CookieService } from 'ngx-cookie-service';
// コンポーネントのインポート
import { AppComponent } from './app.component';
// 共通系コンポーネント
import { HeaderComponent } from './common/header/header.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { FooterComponent } from './common/footer/footer.component';
import { MessagesComponent } from './common/messages/messages.component';
// ページ系コンポーネント
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
// ガード
import { AuthGuard } from './guard/auth.guard';

@NgModule({
  // コンポーネント、ディレクティブ、パイプの登録
  declarations: [
    AppComponent,
    // 共通
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    // ページ
    DashboardComponent,
    LoginComponent,
    MessagesComponent,
  ],
  // モジュールの登録
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrf_token',
      headerName: 'csrf_token',
    }),
  ],
  // DI元となるクラス
  providers: [
    AuthGuard,
    CookieService,
  ],
  // 最初に起動するコンポーネント
  bootstrap: [AppComponent]
})
// @NgModuleの設定情報をクラスとして公開
export class AppModule { }
