// DIを行うためのInjectableデコレーターをインポート
import {Injectable} from '@angular/core';

// デコレーターの宣言
@Injectable()
// クラスの公開
export class DefaultService {
  // 画面間の共通データ
  defaultNumber = 0;

  constructor() {
    console.log('@@@constructor');
  }

  // 受け取った値を保存
  writeDefaultNumber (num: number) {
    console.log('@@@writeDefaultNumber');
    this.defaultNumber = num;
  }

  // 保存した値を読み取り
  readDefaultNumber (): number {
    console.log('@@@readDefaultNumber');
    return this.defaultNumber;
  }
}
