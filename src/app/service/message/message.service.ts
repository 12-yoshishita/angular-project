import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }

  view(message: string) {
    this.messages = [];
    this.messages.push(message);
  }

  viewArray(messages: any) {
    this.messages = messages;
  }

  clear() {
    this.messages = [];
  }

}
