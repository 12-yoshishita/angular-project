import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Logindata } from '../../class/logindata';

import { MessageService } from '../../service/message/message.service';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  private headers: any = new Headers({'Content-Type': 'multipart/form-data'});
  private url = 'https://192.168.138.1/api/v1/login/auth';
  request = new Logindata();
  token;

  constructor(
    private http: HttpClient,
  ) { }

  public postToken(token, model) {
    this.request.csrf_token = token;
    this.request.mail = model.mail;
    this.request.password = model.password;
    return this.http.post(this.url, this.request, {headers: this.headers, responseType: 'text', withCredentials: true});
  }
}
