import { Injectable } from '@angular/core';

const MY_STORAGE_KEY = 'my_storage_key';
@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  public add(token): void {
    localStorage.setItem(MY_STORAGE_KEY, token);
  }

  public fetch(key) {
    return localStorage.getItem(key) || '';
  }

  // 全削除
  clear(): void {
      localStorage.removeItem(MY_STORAGE_KEY);
  }
}
