import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Token } from '../../class/token';

@Injectable({
  providedIn: 'root'
})

export class CsrfService {

  private url = 'https://192.168.138.1/api/v1/csrf/token?type=jsonp';
  csrf;

  constructor(
    private http: HttpClient,
  ) { }

  public getToken() {
    return this.http.jsonp<Token[]>(this.url, 'callback');
  }

  /**
   * 失敗したHttp操作を処理します。
   * アプリを持続させます。
   * @param operation - 失敗した操作の名前
   * @param result - observableな結果として返す任意の値
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: リモート上のロギング基盤にエラーを送信する
      console.error(error); // かわりにconsoleに出力

      // TODO: ユーザーへの開示のためにエラーの変換処理を改善する
      this.log(`${operation} failed: ${error.message}`);

      // 空の結果を返して、アプリを持続可能にする
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
