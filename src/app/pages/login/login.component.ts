import { Component, OnInit } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { CsrfService } from '../../service/auth/csrf.service';
import { LoginService } from '../../service/auth/login.service';
import { StorageService } from '../../service/auth/storage.service';
import { MessageService } from '../../service/message/message.service';


import { Login } from '../../class/login';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  model = new Login();
  csrf;

  constructor(
    private router: Router,
    private csrfService: CsrfService,
    private loginService: LoginService,
    private storageService: StorageService,
    private messageService: MessageService,
  ) {
  }

  ngOnInit() {
    if (this.storageService.fetch('my_storage_key')) {
      this.router.navigate(['dashboard']);
    } else {
      this.getCsrf();
    }
  }

  onSubmit() {
    this.messageService.clear();
    // メール未入力
    if (!this.model.mail) {
      this.messageService.view('メールアドレスは必須です。');
    }
    // パスワード未入力
    if (!this.model.password) {
      this.messageService.view('パスワードは必須です。');
    }

    if (this.model.mail && this.model.password) {
      this.loginService.postToken(this.csrf.token, this.model)
      .toPromise()
      .then((response) => {
        // ログイン成功時処理
        const res = JSON.parse(response);
        this.storageService.add(res.access_token);
        this.router.navigate(['/dashboard']);
      })
      .catch((error) => {
        // ログイン失敗処理
        if (error.status === 406) {
          this.messageService.view('不正な画面遷移が行われました。');
        } else if (error.status === 422) {
          const res = JSON.parse(error.error);
          this.messageService.view('メールアドレスまたはパスワードが正しくありません。');
        }
        this.getCsrf();
      });
    }
  }

  getCsrf() {
    this.csrfService.getToken().subscribe(response => {
      this.csrf = response;
    });
  }
}
