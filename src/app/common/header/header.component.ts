import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StorageService } from '../../service/auth/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private storageService: StorageService,
  ) { }

  ngOnInit() {
  }

  public logout() {
    this.storageService.clear();
    this.router.navigate(['login']);
  }

}
