import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { StorageService } from '../service/auth/storage.service';

const MY_STORAGE_KEY = 'my_storage_key';
@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor (
    private storageService: StorageService,
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.storageService.fetch(MY_STORAGE_KEY)) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
