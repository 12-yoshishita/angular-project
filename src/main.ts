// main.tsが最初に参照されますが、この内容は "/angular.json" に記述されています。
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// 実行モードが開発モードか運用モードかを判定
// デフォルトでは開発モードで実行されるため、環境判定で運用モードで実行するよう指定する
if (environment.production) {
  enableProdMode();
}

// 起動するモジュールの指定
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
